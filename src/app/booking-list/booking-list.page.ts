import { Component, OnInit } from '@angular/core';
import { ConfigService, END_POINT } from '../provider/config.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiProviderService } from '../provider/api-provider.service';


@Component({
  selector: 'app-booking-list',
  templateUrl: './booking-list.page.html',
  styleUrls: ['./booking-list.page.scss'],
})
export class BookingListPage implements OnInit {

  bookings: any[] = [];
  isLoading: boolean = true;

  rootUrl = END_POINT.rootUrl;

  constructor(
    private apiProvider: ApiProviderService,
    private config: ConfigService,
    private loadingCtrl: LoadingController, 
    private toastCtrl: ToastController) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.refresh();
  }

  refresh() {

    this.isLoading = true;
    this.presentLoading()

    this.config.fetchBookings()
        .then(bookings => {
          try {

            let size = bookings.length;
            for (let i = size-1; i >= 0; --i) {
              this.apiProvider.getBooking(bookings[i].id)
                  .subscribe(booking => {
                    this.isLoading = false;
                    this.loadingCtrl.dismiss()

                    booking['ride'] = bookings[i].ride;
                    booking['company'] = bookings[i].company;
                    this.bookings.push(booking);
                  })
            }
            
          } catch (err) {

            this.isLoading = false;
            this.loadingCtrl.dismiss()
            this.presentToast(err)
          }
         
        })
  }

  async presentLoading(message = 'Chargement...') {
    const loading = await this.loadingCtrl.create({
      spinner: 'circles',
      message: `${message}`,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message: `${message}`,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }

}
