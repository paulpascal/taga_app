import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'ride-list/:companyId',
    loadChildren: () => import('./ride-list/ride-list.module').then( m => m.RideListPageModule)
  },
  {
    path: 'ride-detail/:rideId/of/:companyId',
    loadChildren: () => import('./ride-detail/ride-detail.module').then( m => m.RideDetailPageModule)
  },
  {
    path: 'booking/for/:rideId/of/:companyId',
    loadChildren: () => import('./booking/booking.module').then( m => m.BookingPageModule)
  },
  {
    path: 'booking-confirm/:bookingId/ride/:rideId/company/:companyId',
    loadChildren: () => import('./booking-confirm/booking-confirm.module').then( m => m.BookingConfirmPageModule)
  },
  {
    path: 'ticket',
    loadChildren: () => import('./ticket/ticket.module').then( m => m.TicketPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then( m => m.AboutPageModule)
  },
  {
    path: 'booking-list',
    loadChildren: () => import('./booking-list/booking-list.module').then( m => m.BookingListPageModule)
  },
  {
    path: 'tutorial',
    loadChildren: () => import('./tutorial/tutorial.module').then( m => m.TutorialPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
