import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RideListPage } from './ride-list.page';

describe('RideListPage', () => {
  let component: RideListPage;
  let fixture: ComponentFixture<RideListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RideListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RideListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
