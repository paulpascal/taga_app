import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, IonList, AlertController, LoadingController, ToastController, Config } from '@ionic/angular';
import { RideFilterPage } from '../ride-filter/ride-filter.page';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiProviderService } from '../provider/api-provider.service';


@Component({
  selector: 'app-ride-list',
  templateUrl: './ride-list.page.html',
  styleUrls: ['./ride-list.page.scss'],
})
export class RideListPage implements OnInit {

  // Gets a reference to the list element
  @ViewChild('rideList', { static: true }) rideList: IonList;

  ios: boolean;
  dayIndex = 0;
  queryText = '';
  segment = 'all';
  excludeRides: any = [];
  shownRides: any = [];
  groups: any = [];
  ridesGrouped: any = [];
  confDate: string;
  company;
  isLoading: boolean = true;
  defaultHref = '';
  
  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public router: Router,
    public toastCtrl: ToastController,
    public config: Config,
    public apiProvider: ApiProviderService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.ios = this.config.get('mode') === 'ios';
  }

  ionViewDidEnter() {
    this.defaultHref = `/home`;
    this.isLoading = true;
    const companyId = this.route.snapshot.paramMap.get('companyId');
    this.fetchCompany(companyId);
  }

  async presentLoading(message = 'Chargement...') {
    const loading = await this.loadingCtrl.create({
      spinner: 'circles',
      message: `${message}`,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message: `${message}`,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }

  fetchCompany(id) {
    this.presentLoading();
    this.apiProvider.getCompany(id).subscribe(company => {
      this.company = company;
      this.fetchAllRides(false);
    }, error => {
      this.loadingCtrl.dismiss();
      this.presentToast(error);
    });
  }

  fetchAllRides(loading = true) {
    if (loading) { this.presentLoading();     this.isLoading = true; }

    this.apiProvider.getTravels(this.company.id).subscribe(rides => {
      this.ridesGrouped = rides;
      // console.log(this.ridesGrouped);
      this.loadingCtrl.dismiss() // anyhow
      this.isLoading = false;
    }, error => {
      this.loadingCtrl.dismiss();
      this.isLoading = false;
      this.presentToast(error);
    });
  }

  async presentFilter() {
    const modal = await this.modalCtrl.create({
      component: RideFilterPage,
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    console.log(data);

    if (data == -1) this.fetchAllRides();
    else {
      this.apiProvider.getTravelsByDate(this.company.id, data).subscribe(rides => {
        this.ridesGrouped = rides;
        // console.log(this.ridesGrouped);
        this.loadingCtrl.dismiss() // anyhow
        this.isLoading = false;
      }, error => {
        this.loadingCtrl.dismiss();
        this.isLoading = false;
        this.presentToast(error);
      });
    }
  }

  refresh() {
    this.fetchCompany(this.company.id);
  }

}
