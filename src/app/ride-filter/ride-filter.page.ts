import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Config, ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-ride-filter',
  templateUrl: './ride-filter.page.html',
  styleUrls: ['./ride-filter.page.scss'],
})
export class RideFilterPage implements AfterViewInit {
  ios: boolean;

  tracks = [
    { name: 'D\'aujourd\'hui', val: 0, isChecked: false },
    { name: 'De demain', val: 1, isChecked: false },
    { name: 'D\'après demain', val: 2, isChecked: false },
    { name: 'Tout', val: 3, isChecked: true },
  ];
  chosen;

  constructor(
    private config: Config,
    public modalCtrl: ModalController,
    public navParams: NavParams
  ) { }

  ionViewWillEnter() {
    this.ios = this.config.get('mode') === `ios`;
  }

  // TODO use the ionViewDidEnter event
  ngAfterViewInit() {
    // passed in array of track names that should be excluded (unchecked)
    const excludedTrackNames = this.navParams.get('excludedTracks');
    
  }


  applyFilters() {
    // Pass back a new array of track names to exclude
    let data;

    if (this.tracks[3]['isChecked']) {
      // all
      data = -1;
    } else {
      // get the val get travel = to that day
      data = this.chosen;
    }
    
    this.dismiss(data);
  }

  dismiss(data?: any) {
    // using the injected ModalController this page
    // can "dismiss" itself and pass back data
    this.modalCtrl.dismiss(data);
  }

  listen(track, i) {

    if (!track.isChecked) {
      for (var k = 0; k < this.tracks.length; k++) 
        if (k != i ) this.tracks[k]['isChecked'] = false;
      this.chosen = i;
    }
  }

}
