import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { ApiProviderService } from '../provider/api-provider.service';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.page.html',
  styleUrls: ['./booking.page.scss'],
})
export class BookingPage implements OnInit {

  booking: BookingOptions = { firstName: '', lastName: '', tel: '', address: '' };
  submitted = false;
  defaultHref = '';
  ride: any;
  companyId: any;
  rideId: any;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private apiProvider: ApiProviderService
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.companyId = this.route.snapshot.paramMap.get('companyId');
    this.rideId = this.route.snapshot.paramMap.get('rideId');
    this.defaultHref = `/ride-detail/${this.rideId}/of/${this.companyId}`;
    // this.defaultHref = `/booking/for/${rideId}/of/${companyId}`;

    // this.fetchRide(rideId);
  }

  onSubmit(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.presentLoading();
      let data = {
        travel: `/api/v1/travels/${this.rideId}`,
        custFirstName: form.controls.firstName.value,
        custLastName: form.controls.lastName.value,
        custTel: form.controls.tel.value,
        custAddress: form.controls.address.value,
      }
      console.log(data);

      this.apiProvider.makeBooking(data)
          .subscribe((booking: any) => {
            this.loadingCtrl.dismiss();
            this.router.navigateByUrl(`booking-confirm/${booking.id}/ride/${this.rideId}/company/${this.companyId}`)
          }, error => {
            this.loadingCtrl.dismiss();
            this.presentToast(error);
          });

    }
  }

  fetchRide(id) {
    this.presentLoading();
    this.apiProvider.getTravel(id).subscribe(ride => {
      this.ride = ride;
      this.loadingCtrl.dismiss();
    }, error => {
      this.loadingCtrl.dismiss();
      this.presentToast(error);
    });
  }

  async presentLoading(message = 'Chargement...') {
    const loading = await this.loadingCtrl.create({
      spinner: 'circles',
      message: `${message}`,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message: `${message}`,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }

}


interface BookingOptions { 
  firstName: string;
  lastName: string;
  tel: string;
  address: string; 
}