import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiProviderService } from '../provider/api-provider.service';
import { END_POINT } from '../provider/config.service';

@Component({
  selector: 'app-ride-detail',
  templateUrl: './ride-detail.page.html',
  styleUrls: ['./ride-detail.page.scss'],
})
export class RideDetailPage implements OnInit {

  rootUrl = END_POINT.rootUrl;
  defaultHref = '';
  ride: any;
  company: any;
  companyId: any;

  constructor(
    private route: ActivatedRoute,
    public router: Router,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private apiProvider: ApiProviderService
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.companyId = this.route.snapshot.paramMap.get('companyId');
    const rideId = this.route.snapshot.paramMap.get('rideId');
    console.log(rideId);

    this.defaultHref = `/ride-list/${this.companyId}`;

    
    this.fetchCompany(this.companyId);
    this.fetchRide(rideId);
  }

  fetchRide(id) {
    this.presentLoading();
    this.apiProvider.getTravel(id).subscribe(ride => {
      this.ride = ride;
      this.loadingCtrl.dismiss();
    }, error => {
      this.loadingCtrl.dismiss();
      this.presentToast(error);
    });
  }

  fetchCompany(id) {
    this.presentLoading();
    this.apiProvider.getCompany(id).subscribe(company => {
      this.company = company;
      this.loadingCtrl.dismiss();
    }, error => {
      this.loadingCtrl.dismiss();
      this.presentToast(error);
    });
  }


  onRideConfirm() {
    this.router.navigateByUrl(`/booking/for/${this.ride.id}/of/${this.companyId}`);
  }

  async presentLoading(message = 'Chargement...') {
    const loading = await this.loadingCtrl.create({
      spinner: 'circles',
      message: `${message}`,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message: `${message}`,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }

}
