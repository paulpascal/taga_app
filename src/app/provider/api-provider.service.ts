
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators';
import { LoadingController, ToastController } from '@ionic/angular';
import { END_POINT } from './config.service';




@Injectable({
  providedIn: 'root'
})
export class ApiProviderService {

  globalHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(public http: HttpClient, public loadingCtrl: LoadingController, public toastCtrl: ToastController) { }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    let message  = '';
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
      message = 'Verifiez votre connexion SVP.';
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      message = error.error.message;
    }
    // return an observable with a user-facing error message
    return throwError(
      `Une erreur s'est produite; veuillez réessayer plus tard plus tard. ${message?message:''}`);
  }

  
  /* GET all companies array list */
  getCompanies() {
    return this.http
        .get(END_POINT.apiUrl + '/companies.json', {headers:this.globalHeaders})
        // .pipe(map((data: any) => data))
        .pipe(
          retry(2),
          catchError(this.handleError)
        )
    ;
  }

  /* GET company item */
  getCompany(id) {
    return this.http
        .get(END_POINT.apiUrl + `/companies/${id}.json`, {headers:this.globalHeaders})
        .pipe(
          retry(2),
          catchError(this.handleError)
        )
    ;
  }

  /* GET all trajets array list */
  getTravels(id) {
    let d = new Date();
    return this.http
        .get(END_POINT.apiUrl +   `/companies/${id}/travels?sits[gt]=0&date[after]=${d.getFullYear()}-${d.getMonth() +1}-${d.getDate()}&order[date]=asc&order[depHour]=asc`, {headers:this.globalHeaders})
        .pipe(
          // retry(2),
          map(this.processRides, this)
          // catchError(this.handleError)
        );
        // .pipe(
        //   retry(2),
        //   catchError(this.handleError)
        // )
  }

  getTravelsByDate(id, datePlus=0) {
    let d = new Date();
    return this.http
    .get(END_POINT.apiUrl +   `/companies/${id}/travels?date[strictly_before]=${d.getFullYear()}-${d.getMonth()+1}-${d.getDate()+datePlus+1}&date[after]=${d.getFullYear()}-${d.getMonth()+1}-${d.getDate()+datePlus}&sits[gt]=0&order[date]=asc&order[depHour]=asc.json`, {headers:this.globalHeaders})
    .pipe(
          // retry(2),
          map(this.processRides, this)
          // catchError(this.handleError)
        );
        // .pipe(
        //   retry(2),
        //   catchError(this.handleError)
        // )
  }

  processRides(data: any) {
    // just some good 'ol JS fun with objects and arrays
    // build up the data by linking speakers to sessions
    let rides = data['hydra:member'];

    let dates = [];
    let ridesGrouped = [];

    // form dates
    for (let i = 0; i < rides.length; i++) {
      let ride = rides[i];
      if (!dates.includes(ride['date'])) {
        dates.push(ride['date']);
        ridesGrouped.push({'date': ride['date'], 'rides': new Set()})
      } 
    }

    console.log(dates);
    console.log(data);

    // form rides group by date
    for (let i = 0; i < dates.length; i++) {
      for (let j = 0; j < rides.length; j++) {
        let ride = rides[j];
        if (dates[i] === ride['date']) {
          ridesGrouped[i]['rides'].add(ride);
        }
      }
    }
    console.log(ridesGrouped);
    return ridesGrouped;
  }

  /* GET company item */
  getTravel(id) {
    return this.http
        .get(END_POINT.apiUrl + `/travels/${id}.json`, {headers:this.globalHeaders})
        .pipe(
          retry(2),
          catchError(this.handleError)
        )
    ;
  }

  getBooking(id) {
    return this.http
        .get(END_POINT.apiUrl + `/bookings/${id}.json`, {headers:this.globalHeaders})
        .pipe(
          retry(2),
          catchError(this.handleError)
        )
    ;
  }

  makeBooking(data) {
    return  this.http
        .post(END_POINT.apiUrl + '/bookings', JSON.stringify(data), { headers: this.globalHeaders })
        .pipe(
          retry(2),
          catchError(this.handleError)
        )
  }

  confirmBooking(id, amount, payment) {
      return new Promise((resolve, reject) => {
          this.http.get(END_POINT.apiUrl + `/bookings/confirm/${id}/with/${amount}/by/${payment}`, {headers:this.globalHeaders})
              .subscribe(data => {
                  resolve(data)
              }, (err) => {
                  reject(err)
              })
          })
  }

  updateBooking(id, data) {      
      return  this.http
        .post(END_POINT.apiUrl + `/bookings/confirm/${id}`, JSON.stringify(data), {headers:this.globalHeaders})
        .pipe(
          retry(2),
          catchError(this.handleError)
        )
  }

  cancelBooking(id) {
     
      return this.http
          .get(END_POINT.apiUrl + `/bookings/cancel/${id}`, {headers:this.globalHeaders})
          .pipe(
            retry(2),
            catchError(this.handleError)
          )
  }

  processPGPayment(data) {
      // const API_KEY = 'd45269dd-33e1-4d0a-8833-a6bfa9426f32';
      let headers = new HttpHeaders()
      headers = headers.set('Content-Type', 'application/json')
      headers = headers.set('Access-Control-Allow-Origin', '*');
      
      return this.http.post(END_POINT.paygate_v1_pay, JSON.stringify(data), { headers: headers })
          .pipe(
            retry(2),
            catchError(this.handleError)
          )
  }

  checkPGPayment(data) {
    // const API_KEY = 'd45269dd-33e1-4d0a-8833-a6bfa9426f32';
    let headers = new HttpHeaders()
    headers = headers.set('Content-Type', 'application/json')
    headers = headers.set('Access-Control-Allow-Origin', '*');
    
    return this.http.post(END_POINT.paygate_v1_status, JSON.stringify(data), { headers: headers })
        .pipe(
          retry(2),
          catchError(this.handleError)
        )
  }

  checkTGPayment(data) {
    // const API_KEY = 'd45269dd-33e1-4d0a-8833-a6bfa9426f32';
    let headers = new HttpHeaders()
    headers = headers.set('Content-Type', 'application/json')
    headers = headers.set('Access-Control-Allow-Origin', '*');
    
    return this.http.post(END_POINT.apiUrl + `/check`, JSON.stringify(data), { headers: headers })
        .pipe(
          retry(2),
          catchError(this.handleError)
        )
  }

  validateBooking() {
     
    this.http.get(END_POINT.apiUrl + '/pays', {headers:this.globalHeaders})
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

}
