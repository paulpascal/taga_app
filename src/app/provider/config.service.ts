import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';


export enum END_POINT {
  // apiUrl = "http://icar.api.cakket.com/public/api/v1",
  apiUrl = "http://127.0.0.1:8000/api/v1",
  // rootUrl = "http://icar.api.cakket.com/public",
  rootUrl = "http://127.0.0.1:8000",
  
  apilayer_validate = "http://apilayer.net/api/validate",
  paygate_v1_pay = "https://paygateglobal.com/api/v1/pay",
  paygate_v1_status = "https://paygateglobal.com/api/v1/status"
}

export enum KEYS {
  paygate_authtoken = "d45269dd-33e1-4d0a-8833-a6bfa9426f32",
}

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(public storage: Storage) { }

  saveBooking(data) {
    this.fetchBookings().then(items => {
      if (!items) items = [];
      items.push(data);

      this.storage.set('BOOKINGS',  items)
        .then(
          () => {
            console.log('Item Stored');
          },
          error => console.error('Error storing item', error)
        );
      return items;
    })
  }

  fetchBookings() {
    return this.storage.get('BOOKINGS').then((val) => {
      console.log('BOOKINGS: ', val);
      return (val != null)?val:[];
    })
  }
}
