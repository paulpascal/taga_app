import { Component, OnInit } from '@angular/core';
import { ActionSheetController, AlertController, LoadingController, ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiProviderService } from '../provider/api-provider.service';
import { END_POINT, KEYS, ConfigService } from '../provider/config.service';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-booking-confirm',
  templateUrl: './booking-confirm.page.html',
  styleUrls: ['./booking-confirm.page.scss'],
})
export class BookingConfirmPage implements OnInit {

  payment;
  companyId: any;
  rideId: any;
  bookingId: any;
  booking: any;
  ride: any;
  company: any;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private actionCtrl: ActionSheetController,
    private alertCtrl: AlertController,
    private apiProvider: ApiProviderService,
    private callNumber: CallNumber,
    private config: ConfigService
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.companyId = this.route.snapshot.paramMap.get('companyId');
    this.rideId = this.route.snapshot.paramMap.get('rideId');
    this.bookingId = this.route.snapshot.paramMap.get('bookingId');
    // this.defaultHref = `/booking/for/${rideId}/of/${companyId}`;

    this.fetchRide(this.rideId);
    this.fetchBooking(this.bookingId);
    this.fetchCompany(this.companyId);
  }

  fetchBooking(id) {
    this.presentLoading();
    this.apiProvider.getBooking(id).subscribe(booking => {
      this.booking = booking;
      this.loadingCtrl.dismiss();
    }, error => {
      this.loadingCtrl.dismiss();
      this.presentToast(error);
    });
  }

  fetchCompany(id) {
    this.presentLoading();
    this.apiProvider.getCompany(id).subscribe(company => {
      this.company = company;
      this.loadingCtrl.dismiss();
    }, error => {
      this.loadingCtrl.dismiss();
      this.presentToast(error);
    });
  }

  fetchRide(id) {
    this.presentLoading();
    this.apiProvider.getTravel(id).subscribe(ride => {
      this.ride = ride;
      console.log(ride);
      this.loadingCtrl.dismiss();
    }, error => {
      this.loadingCtrl.dismiss();
      this.presentToast(error);
    });
  }

  async presentLoading(message = 'Chargement...') {
    const loading = await this.loadingCtrl.create({
      spinner: 'circles',
      message: `${message}`,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message: `${message}`,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }

  cancelBooking() {
    this.presentLoading('Annulation...')
    this.apiProvider.cancelBooking(this.bookingId).subscribe(resp => {
      console.log(resp);
      this.loadingCtrl.dismiss();
      this.router.navigateByUrl('/home');
    }, error => {
      this.loadingCtrl.dismiss();
      this.presentToast(error);
    });
  }

  openActionSheet() {
    this.actionCtrl.create({
      buttons: [
        {
          text: "Flooz",
          icon: 'checkmark-circle',
          cssClass: 'flooz',
          handler: () => {
            this.payment = "FLOOZ";
            this.presentAlert();
          }
        },
        {
          text: "Tmoney",
          icon: 'checkmark-circle',
          cssClass: 'tmoney',
          handler: () => {
            this.payment = "TMONEY";
            this.presentAlert();
          }
        },
        {
          text: "Annuler",
          icon: 'close',
          role: 'cancel',
          handler: () => {
            this.cancelBooking();
          }

        }
      ]
    }).then(ac => ac.present())
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmer le numéro',
      subHeader: 'Ce numéro sera utilisé pour le paiement.',
      message: 'Si vous voulez continuer avec ce numéro cliquer sur continuer. <br> Sinon modifier puis continuer.',
      inputs: [
        {
          name: 'tel',
          type: 'tel',
          value: `${this.booking.custTel}`
        }
      ],
      buttons: [
        {
          text: "Annuler",
          role: 'cancel',
        },
        {
          text: "Continuer",
          handler: (data) => {
            console.log(data);
            if (!data.tel) {
              this.presentToast('Mettez un numéro valide...')
              return;
            }
            this.booking.paymentTel = data.tel;
            this.saveBooking();
            if (this.payment == 'TMONEY') {
              this.processTmoneyPayment(this.booking)
            } else if (this.payment == 'FLOOZ') {
              this.processFloozPayment(this.booking, data.tel)
            }
          }
        },
      ]
    });
  
    await alert.present();
  }

  processFloozPayment(booking, tel) {
    let data = {
      auth_token: KEYS.paygate_authtoken,
      phone_number: tel,
      amount: this.ride.price,
      description: 'Taga Paiement Réservation',
      identifier: booking.id
    }

    this.presentLoading('Demande de paiement...');
    this.apiProvider.processPGPayment(data)
        .subscribe((res: any) => {
          this.presentToast('Demande de paiement avec succès.')
          console.log(res)
          this.loadingCtrl.dismiss()


          const data2 = { auth_token: KEYS.paygate_authtoken, tx_reference: res.tx_reference	}
          if (res.status == 0) {

            this.presentLoading('Récupération de votre code de voyage...');
            this.apiProvider.checkPGPayment(data2)
                .subscribe((state: any) => {
                  console.log(state);
                  const updateData = {  paymentTel: tel, payment_method: state.payment_method,  status: state.status, tx_reference: state.tx_reference};

                  this.apiProvider.updateBooking(this.booking.id, updateData)
                  .subscribe((updatedBooking: any) => {
                    console.log(updatedBooking)
                    this.loadingCtrl.dismiss()
                    // this.navCtrl.setRoot('BookingPage', { booking: updatedBooking })
                  }, (err) => {
                    console.error(err);
                    this.presentToast('Une erreur s\'est produite, vérifier votre connexion SVP.')
                  })

                }, (err) => {
                  console.error(err);
                  this.loadingCtrl.dismiss()
                  this.presentToast('Une erreur s\'est produite, vérifier votre connexion SVP.')
                })
          } else {
            this.presentToast('Une erreur s\'est produite, vérifier votre connexion SVP.')
          }
        }, (err) => {
          console.error(err);
          this.loadingCtrl.dismiss()
          this.presentToast('Une erreur s\'est produite, vérifier votre connexion SVP.')
        })
  }

  processTmoneyPayment(booking) {
    let data = {
      auth_token: KEYS.paygate_authtoken,
      phone_number: booking.custTel,
      amount: this.ride.price,
      description: 'Taga Paiement Réservation',
      identifier: booking.id
    }

    // this.presentLoading('Demande de paiement...');
    this.callNumber.callNumber('02', true)
    .then(res => {
      console.log('Launched dialer!', res)
      this.loadingCtrl.dismiss()

      this.presentLoading('Récupération de votre code de voyage...');
      this.apiProvider.checkTGPayment({})
          .subscribe((state: any) => {
            console.log(state);
            const updateData = { payment_method: state.payment_method,  status: state.status, tx_reference: state.tx_reference};

            this.apiProvider.updateBooking(this.booking.id, updateData)
            .subscribe((updatedBooking: any) => {
              console.log(updatedBooking)
              this.loadingCtrl.dismiss()
              // this.navCtrl.setRoot('BookingPage', { booking: updatedBooking })
            }, (err) => {
              console.error(err);
              this.loadingCtrl.dismiss()
              this.presentToast('Une erreur s\'est produite, vérifier votre connexion SVP.')
            })

          }, (err) => {
            console.error(err);
            this.loadingCtrl.dismiss()
            this.presentToast('Une erreur s\'est produite, vérifier votre connexion SVP.')
          })
    })
    .catch(async (err) => {
      // this.loadingCtrl.dismiss()
      this.router.navigateByUrl('/ticket')
      console.log('Error launching dialer', err)      
    });
    
  }

  saveBooking() {
    let data = this.booking;
    data['ride'] = this.ride;
    data['company'] = this.company;
    this.config.saveBooking(data);
  }
}
