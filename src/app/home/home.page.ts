import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiProviderService } from '../provider/api-provider.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { END_POINT } from '../provider/config.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  companies: any;
  searchTerm: string = '';
  searching: any = false;
  rootUrl = END_POINT.rootUrl;
  loading;

  constructor(
    public router: Router,
    private apiProvider: ApiProviderService,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController
  ) {  }

  onCompanySelected(company) {
    this.router.navigateByUrl('/ride-list');
  }

  ionViewDidEnter() {

    this.presentLoading();
    this.apiProvider.getCompanies().subscribe((companies: any[]) => {
      this.companies = companies;
      this.loadingCtrl.dismiss();
      // console.log(this.companies);
    }, error => {
      this.loadingCtrl.dismiss();
      this.presentToast(error);
    });
    
  }

  updateCompanies() {
    this.filterCompanies(this.searchTerm)
  }
  
  filterCompanies(searchTerm) {
    this.searching = true;
    for (var i = 0; i < this.companies.length; ++i) {
      if (this.companies[i].name.toLowerCase().indexOf(searchTerm.toLowerCase()) === -1) {
        this.companies[i]['hide'] = true;
        // console.log(this.companies[i], searchTerm)
      } else {
        delete this.companies[i]['hide'];
      }
    }
    this.searching = false;
  }

  async presentLoading(message = 'Chargement...') {
    const loading = await this.loadingCtrl.create({
      spinner: 'circles',
      message: `${message}`,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message: `${message}`,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }

}
